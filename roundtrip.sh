#!/bin/bash

# "--batch --yes" to overwrite, you don't need this

if grep -U $'\015' file.txt > /dev/null; then
  echo "ERROR: File must use LF line endings"
  exit 1
fi

if [[ -z "$(tail -c 1 file.txt)" ]]; then
  echo "ERROR: Last line must NOT end with a LF"
  exit 1
fi

echo "Detach signing..."
gpg --digest-algo SHA512 --armor --detach-sign --text --sig-notation method@m=detach --batch --yes file.txt
sq dearmor file.txt.asc > /tmp/detach

echo "Clearsigning..."
gpg --clearsign --sig-notation method@m=clearsign --batch --yes file.txt
sq dearmor file.txt.asc > /tmp/clearsign

echo "Assembling final file..."
echo "-----BEGIN PGP SIGNED MESSAGE-----" > file.txt.asc
echo "Hash: SHA256" >> file.txt.asc
echo "Hash: SHA512" >> file.txt.asc
echo >> file.txt.asc
cat file.txt >> file.txt.asc
echo >> file.txt.asc
cat /tmp/clearsign /tmp/detach | sq armor >> file.txt.asc

echo "Veryfing..."
gpg --verify-options show-notations --verify --output - file.txt.asc
